import React, { useEffect, useState } from 'react';
import {
  ImageBackground, Text, View
} from 'react-native';
// import Login from './src/screens/login'
import Navigation from "./src/navigation"
//  import Metrics from "./src/theme/metrics"


const App = () => {
  const[isVisible, setIsVisible] = useState(true)
  useEffect(()=>{
    setTimeout(()=>{
      setIsVisible(false)
    },2000)
  },[])

return(
   <Navigation />
)};


export default App;
