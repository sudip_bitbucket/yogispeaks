import axios from "axios";

const instance = axios.create({
    baseURL: 'https://yogispeaks.co.in/Api/'
  });

export default instance;