import * as React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import Login from '../screens/login/homeScreen';
import Home from "../screens/home/homeScreen"
import VideoCall from "../screens/videoCall/homeScreen"
import {COLORS} from '../theme/colors';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {SIZE} from '../theme/fonts';
import {
  HomeIcon,
  Chat,
  Phonecall,
  Wallet,
  Headphones,
  Search,
  Share,
  Bell,
} from '../components/icons';

const Tab = createBottomTabNavigator();

function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="HomeNab"
      screenOptions={{
        headerShown: false,
        tabBarStyle: { height: SIZE.font10 * 6 },
        tabBarActiveTintColor: COLORS.orange,
        }}
    >
      <Tab.Screen
        name="HomeNab"
        component={HomeNab}
        options={{
          tabBarShowLabel: false,
          headerShown: false,
          tabBarIcon: ({color, size}) => <HomeIcon color={color} />,
        }}
      />
      <Tab.Screen
        name="call"
        component={ChatNab}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Chat color={color} />,
        }}
      />
      <Tab.Screen
        name="message"
        component={DetailsScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Phonecall color={color} />,
        }}
      />
      <Tab.Screen
        name="Dsone"
        component={DetailsScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Wallet color={color} />,
        }}
      />
      <Tab.Screen
        name="Dstwo"
        component={DetailsScreen}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Headphones color={color} />,
        }}
      />
    </Tab.Navigator>
  );
}

function DetailsScreen() {
  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text>Details Screen</Text>
    </View>
  );
}

const headerRight = () => {
  return (
      <View style={{
          backgroundColor:COLORS.white,
          width: SIZE.font10 * 5 ,
          height: SIZE.font10 *5 ,
          borderRadius: SIZE.font10 * 5 /2,
          justifyContent: 'center',
          alignItems: 'center'
      
      }}>
                       
      </View>)
}

const Stack = createNativeStackNavigator();
function HomeNab() {
  return (
    <Stack.Navigator >
      <Stack.Screen options={{headerShown: false}} name="home" component={Home} />
    </Stack.Navigator>
  );
}

function ChatNab() {
  return (
    <Stack.Navigator>
      <Stack.Screen options={{headerShown: false}} name="chat" component={DetailsScreen} />
    </Stack.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator 
      screenOptions={{
      headerTintColor: 'white',
      headerStyle: {
        backgroundColor: COLORS.orange
      }
    }}
      >
        <Stack.Screen
          options={{headerShown: false}}
          name="login"
          component={Login}
        />
        <Stack.Screen
          options={{
            headerTitle: () => <Image  source={require("../assets/images/logo.png")} />,
            headerRight: () => headerRight()
          }}
          name="homeTabs"
          component={HomeTabs}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="VideoCall"
          component={VideoCall}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
