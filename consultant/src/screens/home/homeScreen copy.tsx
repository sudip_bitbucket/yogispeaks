// import React from "react";
// import { View, Text } from "react-native";

// const App = () =>{
//     return(<View>
//         <Text>asas</Text>
//     </View>)
// }

// export default App

import React, {useCallback, useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  Alert,
  Switch,
  Modal,
  Pressable,
} from 'react-native';
import {SIZE} from '../../theme/fonts';
import screen from '../../theme/metrics';
import {COLORS} from '../../theme/colors';
import {red} from 'react-native-reanimated/src/reanimated2/Colors';
import Button from '../../components/button';
// import Sound from 'react-native-sound';
const kundliURL = 'https://yogispeaks.co.in/kundali';
const matchURL = 'https://yogispeaks.co.in/kundali-matching';
var Sound = require('react-native-sound');
type gridProps = {
  title: string;
  icon: any;
  color: string;
  navigation: any;
  navigationPath: string;
  navigationType: string;
};

const Grid: React.FC<gridProps> = ({
  children,
  title,
  icon,
  color,
  navigation,
  navigationPath,
  navigationType,
}) => {
  let iconPath = require('../../assets/icons/telephone.png');
  switch (icon) {
    case 'telephone':
      iconPath = require('../../assets/icons/telephone.png');
      break;
    case 'comment':
      iconPath = require('../../assets/icons/comment.png');
      break;
    case 'kundli':
      iconPath = require('../../assets/icons/kundli.png');
      break;
    case 'esoteric':
      iconPath = require('../../assets/icons/esoteric.png');
      break;
    case 'jigsaw':
      iconPath = require('../../assets/icons/jigsaw.png');
      break;
    case 'horoscope':
      iconPath = require('../../assets/icons/horoscope.png');
      break;
  }

  const handlePress = useCallback(async () => {
    const supported = await Linking.canOpenURL(navigationPath);
    if (supported) {
      await Linking.openURL(navigationPath);
    } else {
      Alert.alert(`Don't know how to open this URL: ${navigationPath}`);
    }
  }, [navigationPath]);

  return (
    <TouchableOpacity
      onPress={navigationType === 'app' ? () => navigation(true) : handlePress}
      style={[styles.col, {backgroundColor: color}]}>
      <View style={{flex: 2, justifyContent: 'center'}}>
        <Image
          style={{width: SIZE.font12 * 4, height: SIZE.font12 * 4}}
          source={iconPath}
        />
      </View>
      <View style={{flex: 1}}>
        <Text style={styles.gridText}>{title}</Text>
      </View>
      {/* <View style={{position: "absolute", paddingLeft: SIZE.font20 * 4, paddingBottom: SIZE.font20 * 4 }}><Text>s</Text></View> */}
    </TouchableOpacity>
  );
};

type tableProps = {
  titleText: string;
  nameText: string;
};

const Table: React.FC<tableProps> = ({children, titleText, nameText}) => {
  return (
    <View style={styles.rowCall}>
      <View style={styles.collCall1}>
        <Text style={styles.titleText}>{titleText}:</Text>
      </View>
      <View style={styles.collCall2}>
        <Text style={styles.nameText}>{nameText}</Text>
      </View>
    </View>
  );
};

// require('../../assets/sound/airtale.mp3')
const Home = (props: any) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [isEnabled, setIsEnabled] = useState(false);
  // const [whoosh, setWhoosh] = useState(null);
  let whoosh;
  useEffect(() => {
    Sound.setCategory('Playback', true);
    setTimeout(()=>{
      playSound()
      setModalVisible(true);
    },2000)
    setTimeout(()=>{
      stopSound()
      setModalVisible(false);
    },4000)
    setTimeout(()=>{
      playSound()
      setModalVisible(true);
    },5000)
    setTimeout(()=>{
      stopSound()
      setModalVisible(false);
    },7000)
    return () => {
      if (whoosh) whoosh.release();
    };
  }, []);

  // const playSound = () => {
  //   sound1 = new Sound(
  //     require('../../assets/sound/airtale.mp3'),
  //     (error, _sound) => {
  //       if (error) {
  //         alert('error' + error.message);
  //         return;
  //       }
  //       sound1.play(() => {
  //         sound1.release();
  //       });
  //     },
  //   );
  // };
const playSound = () =>{
  console.log('playSound');
  whoosh = new Sound(
        require('../../assets/sound/airtale.mp3'),
        (error, _sound) => {
          if (error) {
            alert('error' + error.message);
            return;
          }
          whoosh.play(() => {
            console.log('play whoosh>>>',whoosh)
            // setWhoosh(whoosh)
            whoosh.release();
          });
        },
      );
      // setWhoosh(whoosh)
}

  const stopSound = () => {
    console.log('whoosh stop>>', whoosh)
    if (whoosh) {
      whoosh.stop(() => {
        console.log('Stop');
      });
    }
  };

  const _startRing = () => {
    setModalVisible(true);
    playSound()
  };

  const _acceptCall = () => {
    setModalVisible(false);
    stopSound()
  };

  const _rejectCall = () => {};

  const toggleSwitch = () => setIsEnabled(previousState => !previousState);
  const {navigation} = props;
  return (
    <SafeAreaView style={{flex: 1, backgroundColor: COLORS.white}}>
      <View style={styles.container}>
        <View style={styles.header}>
          <View>
            <Text style={{fontSize: SIZE.font20, color: COLORS.orange}}>
              Your Status
            </Text>
          </View>
          <View>
            <Switch
              trackColor={{false: '#767577', true: COLORS.orange}}
              thumbColor={isEnabled ? COLORS.callGrid : COLORS.white}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.row}>
            <Grid
              navigation={_startRing}
              navigationPath="AstrologerList"
              navigationType="app"
              color={COLORS.callGrid}
              title="Tolk to Consultant"
              icon="telephone"
            />
            <Grid
              navigation={navigation}
              navigationPath="AstrologerList"
              navigationType="app"
              color={COLORS.chatGrid}
              title="Tolk to Consultant"
              icon="comment"
            />
            <Grid
              navigation={navigation}
              navigationPath={kundliURL}
              navigationType="webside"
              color={COLORS.kundaliGrid}
              title="Kundli"
              icon="kundli"
            />
          </View>
          <View style={styles.row}>
            <Grid
              navigation={navigation}
              navigationPath="AstrologerList"
              navigationType="app"
              color={COLORS.predictionGrid}
              title="Deily Prediction"
              icon="esoteric"
            />
            <Grid
              navigation={navigation}
              navigationPath={matchURL}
              navigationType="webside"
              color={COLORS.matchingGrid}
              title="Match Making"
              icon="jigsaw"
            />
            <Grid
              navigation={navigation}
              navigationPath="zodiac"
              navigationType="app"
              color={COLORS.zodiacGrid}
              title="Zodiac"
              icon="horoscope"
            />
          </View>
        </View>
        {/* <View style={styles.footer}> */}
        {/* <Image style={{width: screen.screenWidth, height: SIZE.font20 * 7}} source={require("../../assets/images/add_bnner.png")} /> */}
        {/* </View> */}
      </View>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modalBody}>
              <Table titleText="Name" nameText="Adrish khan" />
              <Table titleText="DOB" nameText="23-07-2015" />
              <Table titleText="Time" nameText="Adrish khan" />
              <Table titleText="Location" nameText="Adrish khan" />
            </View>
            <View style={styles.modalFooter}>
              <Button
                buttonIcon={false}
                handleAction={_acceptCall}
                title="Accept Call"
                viewStyle={{
                  backgroundColor: COLORS.zodiacGrid,
                  borderColor: COLORS.zodiacGrid,
                }}
              />
              <Button
                buttonIcon={false}
                handleAction={_rejectCall}
                title="Reject Call"
                viewStyle={{
                  backgroundColor: COLORS.callGrid,
                  borderColor: COLORS.callGrid,
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    //    backgroundColor: 'red'
  },
  body: {
    padding: SIZE.font6,
  },
  footer: {},
  row: {
    flexDirection: 'row',
  },
  col: {
    flex: 1,
    backgroundColor: 'orange',
    margin: SIZE.font6,
    borderRadius: SIZE.font8,
    height: (screen.screenWidth - SIZE.font6 * 8) / 3,
    justifyContent: 'center',
    alignItems: 'center',
    padding: SIZE.font6,
  },
  gridText: {
    color: COLORS.white,
    fontSize: SIZE.font12,
    textAlign: 'center',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.3)',
  },
  modalView: {
    backgroundColor: COLORS.white,
    borderRadius: SIZE.font12,
    padding: SIZE.font12,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: screen.screenWidth - SIZE.font12 * 4,
  },
  modalBody: {
    //   backgroundColor: 'red'
  },
  modalFooter: {
    // backgroundColor: 'green',
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    width: '100%',
  },
  rowCall: {
    flexDirection: 'row',
    //   justifyContent: "space-around",
    width: '80%',
  },
  collCall1: {
    flex: 5,
    padding: SIZE.font6,
    // width: ""
    // backgroundColor: 'yellow'
  },
  collCall2: {
    flex: 7,
    padding: SIZE.font6,
    // backgroundColor: 'gray'
  },
  titleText: {
    color: COLORS.black,
    fontSize: SIZE.font16,
  },
  nameText: {
    color: COLORS.black,
    fontSize: SIZE.font16,
  },
});

export default Home;
