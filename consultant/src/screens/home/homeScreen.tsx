import React, {useCallback, useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Linking,
  Alert,
  Switch,
  Pressable,
} from 'react-native';
import {SIZE} from '../../theme/fonts';
import screen from '../../theme/metrics';
import {COLORS} from '../../theme/colors';
import Button from '../../components/button';
import Sound from 'react-native-sound';
import Modal from 'react-native-modal';
import API from '../../utils/_axios';
// import Video from './videoScreen';
import AsyncStorage from '@react-native-community/async-storage';

const kundliURL = 'https://yogispeaks.co.in/kundali';
const matchURL = 'https://yogispeaks.co.in/kundali-matching';
type tableProps = {
  titleText: string;
  nameText: string;
};

const Table: React.FC<tableProps> = ({children, titleText, nameText}) => {
  return (
    <View style={styles.rowCall}>
      <View style={styles.collCall1}>
        <Text style={styles.titleText}>{titleText}:</Text>
      </View>
      <View style={styles.collCall2}>
        <Text style={styles.nameText}>{nameText}</Text>
      </View>
    </View>
  );
};

type gridProps = {
  title: string;
  icon: any;
  color: string;
  navigation: any;
  navigationPath: string;
  navigationType: string;
};

type callDetails = {
  id: number
}

const Grid: React.FC<gridProps> = ({
  children,
  title,
  icon,
  color,
  navigation,
  navigationPath,
  navigationType,
}) => {
  let iconPath = require('../../assets/icons/telephone.png');
  switch (icon) {
    case 'telephone':
      iconPath = require('../../assets/icons/telephone.png');
      break;
    case 'comment':
      iconPath = require('../../assets/icons/comment.png');
      break;
    case 'kundli':
      iconPath = require('../../assets/icons/kundli.png');
      break;
    case 'esoteric':
      iconPath = require('../../assets/icons/esoteric.png');
      break;
    case 'jigsaw':
      iconPath = require('../../assets/icons/jigsaw.png');
      break;
    case 'horoscope':
      iconPath = require('../../assets/icons/horoscope.png');
      break;
  }

  const handlePress = useCallback(async () => {
    const supported = await Linking.canOpenURL(navigationPath);
    if (supported) {
      await Linking.openURL(navigationPath);
    } else {
      Alert.alert(`Don't know how to open this URL: ${navigationPath}`);
    }
  }, [navigationPath]);

  return (
    <TouchableOpacity
      onPress={navigationType === 'app' ? () => navigation(true) : handlePress}
      style={[styles.col, {backgroundColor: color}]}>
      <View style={{flex: 2, justifyContent: 'center'}}>
        <Image
          style={{width: SIZE.font12 * 4, height: SIZE.font12 * 4}}
          source={iconPath}
        />
      </View>
      <View style={{flex: 1}}>
        <Text style={styles.gridText}>{title}</Text>
      </View>
      {/* <View style={{position: "absolute", paddingLeft: SIZE.font20 * 4, paddingBottom: SIZE.font20 * 4 }}><Text>s</Text></View> */}
    </TouchableOpacity>
  );
};

const initialCallDetails = {
  id:0
}

const Home = (props: any) => {
  let sound1: any;
  let nIntervId;
  const {navigation} = props;
  // const {user_id} = props.route.params;
  const [isEnabled, setIsEnabled] = useState(false);
  const [isModalVisible, setModalVisible] = useState(false);
  const [videoCallStatus, setVideoCallStatus] = useState(false)
  const [callDetails, setCallDetails] = useState(initialCallDetails)
  const [callDetailsStatus, setCallDetailsStatus] = useState(false)
  useEffect(() => {
    Sound.setCategory('Playback', true)
    // setTimeout(() => {
    //   toggleModal();
    // }, 2000);
    // setTimeout(() => {
    //   _incomingCallSession();
    // }, 2000);
    _callInterval()
    return () => {
      if (sound1) sound1.release();
    };
  }, []);

  useEffect(() => {
    if (isModalVisible === true) {
      console.log('isModalVisible');
      playSound();
    }
  }, [isModalVisible]);

  const _callInterval = () => {
    if (!nIntervId) {
      nIntervId = setInterval(_incomingCallSession(), 2000);
    }
  }

  const _getUserId = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      return value !== null ? value : null
    } catch (error) {
      // Error retrieving data
    }
  };

  const _incomingCallSession = async () => {
    const userId = await _getUserId() 
    const response = await API.post('incoming_call_session', {user_id: userId});
    if(!!response && response.data.status == 1){
      // setCallDetailsStatus(true)
      setCallDetails(response.data.details)
        console.log('>>>>>',response.data)
        toggleModal();
        clearInterval(nIntervId);
        nIntervId = null; 
    }
    else
      _callInterval()
        // console.log(response.data.message)
  }

  const _acceptCallSession = async () => {
    const response = await API.post('accept_call_session', {call_session_id: callDetails.id, is_accept_invitation: 1});
    if(!!response && response.data.status == 1)
      navigation.navigate('VideoCall')
        // console.log(response.data)
    else
        console.log(response.data.message)
  }

  const _rejectCallSession = async () => {
    const response = await API.post('reject_call_session', {call_session_id: callDetails.id});
    if(!!response && response.data.status == 1)
        // _callInterval()
        console.log(response.data)
    else
        console.log(response.data.message)
  }

  const playSound = () => {
    sound1 = new Sound(
      require('../../assets/sound/airtale.mp3'),
      (error, _sound) => {
        if (error) {
          Alert.alert('error' + error.message);
          return;
        }
        sound1.play(() => {
          sound1.release();
        });
      },
    );
  };

  const stopSound = () => {
    if (sound1) {
      sound1.stop(() => {
        console.log('Stop');
      });
    }
  };
  const _acceptCall = () => {
    stopSound();
    toggleModal();
    _acceptCallSession()
    // navigation.navigate('VideoCall')
  };

  const _rejectCall = () => {
    stopSound();
    toggleModal();
    _callInterval()
    _rejectCallSession()
  };
  const toggleSwitch = () => setIsEnabled(previousState => !previousState)

  const toggleModal = () => {
    console.log('isModalVisible')
    setModalVisible(!isModalVisible);
  };
  return (
      <SafeAreaView style={{flex: 1, backgroundColor: COLORS.white}}>
      <View style={styles.container}>
        <View style={styles.header}>
          <View>
            <Text style={{fontSize: SIZE.font20, color: COLORS.orange}}>
              Your Status
            </Text>
          </View>
          <View>
            <Switch
              trackColor={{false: '#767577', true: COLORS.orange}}
              thumbColor={isEnabled ? COLORS.callGrid : COLORS.white}
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>
        <View style={styles.body}>
          <View style={styles.row}>
            <View style={[styles.col, {backgroundColor: COLORS.callGrid}]}>
              <View style={{flex: 2, justifyContent: 'center'}}>
                <Image
                  style={{width: SIZE.font12 * 4, height: SIZE.font12 * 4}}
                  source={require('../../assets/icons/telephone.png')}
                />
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.gridText}>Tolk to Consultant</Text>
              </View>
            </View>
            <View style={[styles.col, {backgroundColor: COLORS.chatGrid}]}>
              <View style={{flex: 2, justifyContent: 'center'}}>
                <Image
                  style={{width: SIZE.font12 * 4, height: SIZE.font12 * 4}}
                  source={require('../../assets/icons/comment.png')}
                />
              </View>
              <View style={{flex: 1}}>
                <Text style={styles.gridText}>Tolk to Consultant</Text>
              </View>
            </View>
            <Grid
              navigation={navigation}
              navigationPath={kundliURL}
              navigationType="webside"
              color={COLORS.kundaliGrid}
              title="Kundli"
              icon="kundli"
            />
          </View>
          <View style={styles.row}>
            <Grid
              navigation={navigation}
              navigationPath="AstrologerList"
              navigationType="app"
              color={COLORS.predictionGrid}
              title="Deily Prediction"
              icon="esoteric"
            />
            <Grid
              navigation={navigation}
              navigationPath={matchURL}
              navigationType="webside"
              color={COLORS.matchingGrid}
              title="Match Making"
              icon="jigsaw"
            />
            <Grid
              navigation={navigation}
              navigationPath="zodiac"
              navigationType="app"
              color={COLORS.zodiacGrid}
              title="Zodiac"
              icon="horoscope"
            />
          </View>
        </View>
      </View>
      <Modal isVisible={isModalVisible}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modalBody}>
              <Table titleText="Name" nameText="Adrish khan" />
              <Table titleText="DOB" nameText="23-07-2015" />
              <Table titleText="Time" nameText="Adrish khan" />
              <Table titleText="Location" nameText="Adrish khan" />
            </View>
            <View style={styles.modalFooter}>
              <Button
                buttonIcon={false}
                handleAction={_acceptCall}
                title="Accept Call"
                viewStyle={{
                  backgroundColor: COLORS.zodiacGrid,
                  borderColor: COLORS.zodiacGrid,
                }}
              />
              <Button
                buttonIcon={false}
                handleAction={_rejectCall}
                title="Reject Call"
                viewStyle={{
                  backgroundColor: COLORS.callGrid,
                  borderColor: COLORS.callGrid,
                }}
              />
            </View>
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

export default Home;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-evenly',
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignItems: 'center',
  },
  body: {
    padding: SIZE.font6,
  },
  footer: {},
  row: {
    flexDirection: 'row',
  },
  col: {
    flex: 1,
    backgroundColor: 'orange',
    margin: SIZE.font6,
    borderRadius: SIZE.font8,
    height: (screen.screenWidth - SIZE.font6 * 8) / 3,
    justifyContent: 'center',
    alignItems: 'center',
    padding: SIZE.font6,
  },
  gridText: {
    color: COLORS.white,
    fontSize: SIZE.font12,
    textAlign: 'center',
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalView: {
    backgroundColor: COLORS.white,
    borderRadius: SIZE.font12,
    padding: SIZE.font12,
    alignItems: 'center',
    shadowRadius: 4,
    elevation: 5,
    width: screen.screenWidth - SIZE.font12 * 4,
  },
  modalBody: {
    //   backgroundColor: 'red'
  },
  modalFooter: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    width: '100%',
  },
  rowCall: {
    flexDirection: 'row',
    width: '80%',
  },
  collCall1: {
    flex: 5,
    padding: SIZE.font6,
  },
  collCall2: {
    flex: 7,
    padding: SIZE.font6,
  },
  titleText: {
    color: COLORS.black,
    fontSize: SIZE.font16,
  },
  nameText: {
    color: COLORS.black,
    fontSize: SIZE.font16,
  },
});
