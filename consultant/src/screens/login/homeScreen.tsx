// import React from "react";
// import { StyleSheet, Text, View } from "react-native";

// const Home = () => {

//     return(<View>
//         <Text>Login ddd</Text>
//     </View>)
// } 

// const styles = StyleSheet.create({
//     container:{
//         flex: 1,
//         backgroundColor: 'red',
//         width: 100,
//         height: 100
//     }
// })

// export default Home

import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  TextInput,
  TouchableOpacity,
  Pressable,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import screen from '../../theme/metrics';
import {SIZE, WEIGHT} from '../../theme/fonts';
import {COLORS} from '../../theme/colors';
import {ArrowRight} from '../../components/icons';
import Button from '../../components/button';
import {initialFields} from './config';
// import {validateLogin} from '../../store/actions/login';
const axios = require('axios');

const initialProfole = {
  user_id: "",
  fname: '',
  lname: '',
  dob: ''
}

const App = (props: any) => {
  const isDarkMode = useColorScheme() === 'dark';
  const [fields, setFields] = useState({...initialFields});
  const [profile, setProfile] = useState(initialProfole);

  useEffect(() => {
    if(profile.user_id != '')
    _storeData();
  }, [profile]);

  const _handleChange = (name: any) => (value: any) => {
    // console.log(fields);
    setFields({...fields, [name]: value});
  };

  const _handleSignin = () => {
    console.log('_handleSignin')
    const fieldsObj = {...fields}
    console.log('fieldsObj', fieldsObj)
    axios({
      method: 'post',
      url: 'https://yogispeaks.co.in/Api/login',
      // data: {...fields},
      data: {...fieldsObj},
    }).then((response: any) => {
      // console.log('_handleSignin', response)
      if (response.data.status === '1') {
        setProfile(response.data.user_details);
      } else {
      }
    });
  };

  const _storeData = async () => {
    const {user_id, fname, lname, dob} = profile
    try {
      await AsyncStorage.setItem('user_id', user_id);
      await AsyncStorage.setItem('fname', fname);
      await AsyncStorage.setItem('lname', lname);
      await AsyncStorage.setItem('dob', dob);
      props.navigation.navigate('homeTabs')
    } catch (error) {
      // Error saving data
    }
  };

  const _retrieveData = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
      if (value !== null) {
        // We have data!!
        // console.log(value);
      }
    } catch (error) {
      // Error retrieving data
    }
  };

  return (
    <SafeAreaView style={{flex: 1}}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <View style={styles.container}>
        <View style={{flex: 1}}>
          <View
            style={{
              backgroundColor: COLORS.orange,
              // flex: 5,
              height: screen.screenHeight * 5/12,
              borderBottomLeftRadius: SIZE.font20 * 20,
              borderBottomStartRadius: SIZE.font20 * 7,
            }}></View>
          <View style={{backgroundColor: '#fff', flex: 7}}></View>
        </View>
        <View
          style={{
            top: screen.screenWidth * 0.1,
            left: screen.screenWidth * 0.05,
            width: screen.screenWidth * 0.9,
            //  height: 300,
            position: 'absolute',
          }}>
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Image source={require('../../assets/images/logo.png')} />
          </View>
          <View
            style={{
              marginTop: screen.screenWidth * 0.1,
              backgroundColor: COLORS.backgroundColor,
              height: screen.screenHeight * 0.6,
              borderRadius: SIZE.font10,
              padding: SIZE.font20,
            }}>
            {/* <ScrollView> */}
            <View>
              <View style={styles.inputView}>
                <TextInput
                  value={fields.login_id}
                  style={styles.textInput}
                  placeholder="Email"
                  placeholderTextColor={COLORS.textPrimery}
                  onChangeText={_handleChange('login_id')}
                />
              </View>
              <View style={styles.inputView}>
                <TextInput
                  secureTextEntry={true}
                  style={styles.textInput}
                  placeholder="Password"
                  placeholderTextColor={COLORS.textPrimery}
                  onChangeText={_handleChange('password')}
                />
              </View>
            </View>
            <View style={styles.buttonCon}>
              {/* <Pressable style={styles.buttonView}>
                <Text style={styles.buttonText}>SIGN IN</Text>
                <ArrowRight />
              </Pressable> */}
              <Button handleAction={() => _handleSignin()} title="SIGN IN" />
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  margin: SIZE.font12,
                }}>
                <Text
                  style={{
                    color: COLORS.black,
                    fontSize: SIZE.font12,
                  }}>
                  Now to Yogi speaks?{' '}
                  <Text
                    style={{
                      color: 'blue',
                    }}>
                    Please Register
                  </Text>
                </Text>
              </View>
            </View>
            {/* </ScrollView> */}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  inputView: {
    borderWidth: 1,
    borderColor: COLORS.orange,
    borderRadius: (SIZE.font14 * 3) / 2,
    width: '100%',
    height: SIZE.font14 * 3,
    marginBottom: SIZE.font12,
  },
  textInput: {
    flex: 1,
    padding: SIZE.font10,
    marginLeft: 20,
  },

  buttonCon: {
    paddingVertical: SIZE.font10,
  },
});

export default App;
