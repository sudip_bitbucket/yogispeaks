import React from "react";
import {Pressable, Text, StyleSheet} from "react-native"
import {ArrowRight} from '../components/icons'
import { COLORS } from '../theme/colors';
import { SIZE} from '../theme/fonts';
const Button:React.FC<{
    viewStyle?: Object,
    textStyle?: Object,
    buttonIcon?: boolean
    title: string,
    handleAction: () => any
  }> = ({children, handleAction, title, viewStyle, textStyle, buttonIcon = true}) =>(
    <Pressable onPress={() => handleAction()} style={[styles.buttonView, viewStyle]}>
        <Text style={[styles.buttonText, textStyle]}>{title}</Text>
        {buttonIcon && <ArrowRight /> }
    </Pressable>
)

export default Button;

const styles = StyleSheet.create({
    buttonView:{
        flexDirection: 'row',
        marginTop: SIZE.font20,
        borderWidth: 1,
        borderColor: COLORS.callGrid,
        paddingVertical: SIZE.font10,
        paddingHorizontal: SIZE.font12,
        borderRadius: SIZE.font20 + 2,
        backgroundColor: COLORS.callGrid,
        justifyContent: "space-between",
        alignItems: 'center'
      },
      buttonText:{
        fontSize: SIZE.font16,
        color: COLORS.white
    },
})