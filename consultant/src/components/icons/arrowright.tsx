import * as React from 'react'
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS} from "../../theme/colors"
import {SIZE} from "../../theme/fonts"

const ArrowRight :React.FC<{
    size?: number,
    color?: string
  }> = ({children, size, color}) => 
    <Icon 
    name="arrowright" 
    size={size || SIZE.font20} 
    color={color || COLORS.white} />


export default ArrowRight