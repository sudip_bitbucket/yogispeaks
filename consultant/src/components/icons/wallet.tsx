import * as React from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS} from "../../theme/colors"
import {SIZE} from "../../theme/fonts"

const Home:React.FC<{
    size?: number ,
    color?: string
  }> = ({children, size, color}) => 
    <Icon 
    name="wallet-outline" 
    size={size || SIZE.font14 * 2} 
    color={color || COLORS.orange} />


export default Home