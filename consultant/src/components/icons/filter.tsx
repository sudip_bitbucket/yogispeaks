import * as React from 'react'
import Icon from 'react-native-vector-icons/AntDesign';
import {COLORS} from "../../theme/colors"
import {SIZE} from "../../theme/fonts"

const Home:React.FC<{
    size?: number ,
    color?: string
  }> = ({children, size, color}) =>
    <Icon 
    name="filter" 
    size={size || SIZE.font14 * 2} 
    color={color || COLORS.orange} />


export default Home