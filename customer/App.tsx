 import React, { useEffect, useState } from 'react';
 import {
   ImageBackground
 } from 'react-native';
 import Navigation from "./src/navigation"
 import Metrics from "./src/theme/metrics"
 
 const App = () => {
   const[isVisible, setIsVisible] = useState(true)
   useEffect(()=>{
     setTimeout(()=>{
       setIsVisible(false)
     },2000)
   },[])
 
 return(<>
     <Navigation/>
     {
       isVisible === true && <ImageBackground style={{width:Metrics.screenWidth, height: Metrics.screenHeight}} source={require("./src/assets/images/splash.png")} />
     }
 </>)};
 
 
 export default App;
 