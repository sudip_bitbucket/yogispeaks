import React, { useEffect, useState } from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   Image,
   TouchableOpacity,
   FlatList
 } from 'react-native';
 import AsyncStorage from '@react-native-community/async-storage';
import { SIZE } from '../../theme/fonts'
import screen from '../../theme/metrics'
import { COLORS } from '../../theme/colors';
import {Filter as FilterIcon, Down, Star, Language, Right, Rupee,Staro} from '../../containers/icons'
const axios = require('axios');

const Filter:React.FC<{
    title: string;
  }> = ({children, title}) =>(
    <View style={styles.filterItem}>
        <View style={{justifyContent: 'center'}}><Text style={{fontSize:SIZE.font12}}>{title}</Text></View>
        <View><Down/></View>
    </View>)
const Description:React.FC<{
    title: string;
  }> = ({children, title}) =>(
    <>
        <View style={styles.icon}>{children}</View>
        <View style={styles.title}>
            <Text style={styles.titleText}>{title}</Text>
        </View>
    </>)

const Item = (item: any) => {
    
    const {fname, lname, abbreviation, experiance, profile_image, call_rate, languages, rating, user_id} = item.data
    const profileImage = item.imagePath+''+profile_image
    return (<TouchableOpacity onPress={() => item.navigation.navigate('AstrologerDetails',{ consultantId: user_id, channel: null })} style={styles.row} >
        <View style={styles.cal1}>
            <View style={styles.avatar}>
            <Image 
                style={styles.avatarImage} 
                source = {{ uri: profileImage }}
             />
            </View>
            <View style={styles.stars}>
                {/* <Star />
                <Star />
                <Star />
                <Star />
                <Star /> */}
                {
                    [1,2,3,4,5].map((item,index) =>{
                        const dd = parseInt(rating)
                        return (!!dd && dd > index ? 
                        <Star key={index} />
                        :
                        <Staro key={index} />)
                    })
                }
            </View>
        </View>
        <View style={styles.cal2}>
            <View style={styles.cal2row1}>
                <View><Text style={styles.nameText}>{abbreviation +' '+ fname + ' ' + lname}</Text></View>
                <View><Right/></View>
            </View>
            <View style={styles.description}>
                <View style={styles.cal2row2}>
                    <View style={styles.row2col1}>
                        <Description title='Vedic Astrology' >
                            <Image style={{width: SIZE.font20, height: SIZE.font20}} source={require("../../assets/icons/astrology.png")} />
                        </Description>
                    </View>
                    <View style={styles.row2col2}>
                        <Description title={experiance + ' year(s)'} >
                            <Image style={{width: SIZE.font20, height: SIZE.font20}} source={require("../../assets/icons/experience.png")} />
                        </Description>
                    </View>
                </View>
                <View style={styles.cal2row2}>
                <View style={styles.row2col1}>
                        <Description title={languages} >
                            <Image style={{width: SIZE.font20, height: SIZE.font20}} source={require("../../assets/icons/language.png")} />
                        </Description>
                    </View>
                    <View style={styles.row2col2}>
                        <Description title={call_rate +'/m'} >
                            <Rupee />
                        </Description>
                    </View>
                </View>
            </View>
        </View>
    </TouchableOpacity>)
  };

 const Index = (props: any) => {
     const [consultants, setConsultents] = useState([])
     const [imagePath, setImagePath] = useState('')
     useEffect(()=>{
        //  console.log('nav name>', props.navigation)
        _retrieveData()
     },[])

     const _retrieveData = async () => {
        try {
          const value = await AsyncStorage.getItem('user_id');
        //   console.log(value);
          if (value !== null) {
            // console.log(value);
            _getConsultantList(value)
          }
        } catch (error) {
          // Error retrieving data
        }
      };

    const _getConsultantList = (user_id: number | string) =>{
        axios({
            method: 'post',
            url: 'https://yogispeaks.co.in/Api/consultant_listing',
            data: {user_id},
          }).then((response: any) => {
            // console.log(response.data.consultants);
            if (response.data.status === '1') {
                // console.log('sss>>>',response.data.consultants)
              setConsultents(response.data.consultants);
              setImagePath(response.data.user_image_url)
            } else {
            }
          });
    }
    const renderItem = ( data :any) => (
        <Item navigation={props.navigation} data={data.item} imagePath={imagePath} />
      );
   return (<SafeAreaView style={{flex: 1}}>
       <View style={{flex: 1}}>
           <View style={styles.addvise}><Image style={{width: screen.screenWidth, height: SIZE.font20 * 6}} source={require("../../assets/images/add_bnner.png")} /></View>
           <View style={styles.filter}>
               <View><FilterIcon /></View>
               <View style={styles.filterItems}>
                    {<Filter title="Expc"/>}
                    {<Filter title="Lang"/>}
                    {<Filter title="Spec"/>}
               </View>
           </View>
           <View style={styles.main}>
               
               <FlatList
                data={consultants}
                renderItem={renderItem}
                keyExtractor={item => item.id}
            />
            </View>
       </View>
       </SafeAreaView>)
 };
 
 const styles = StyleSheet.create({
   nav: {
       flex: 1,
       backgroundColor: COLORS.red
   },
   addvise: {
    flex: 2
    },
    filter: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: SIZE.font6,
        backgroundColor: COLORS.primery,
    },
    main:{
        flex: 8,
        backgroundColor: COLORS.primery,
        padding: SIZE.font6
    },
    filterItems: {
        flexDirection: 'row',
        
    },
    filterItem: {
        flexDirection: 'row',
        marginHorizontal: SIZE.font6,
        paddingHorizontal: SIZE.font6,
        paddingVertical: SIZE.font6 / 2,
        borderWidth: 1,
        borderColor: COLORS.orange,
        borderRadius: SIZE.font20
    },
    row: {
        flexDirection: 'row',
        backgroundColor: COLORS.gray,
        borderRadius: SIZE.font10,
        margin: SIZE.font6,
        padding: SIZE.font6,
    },
    cal1: {
        justifyContent: 'center',
        alignItems: 'center',
        padding: SIZE.font6,
    },
    cal2: {
        flex: 1,
        paddingHorizontal: SIZE.font6,
    },
    avatar:{
        backgroundColor:COLORS.white,
        width: SIZE.font20 * 3,
        height: SIZE.font20 * 3,
        borderRadius: SIZE.font10 * 3,
        borderWidth: 3,
        borderColor: "green",
        justifyContent: 'center',
        alignItems: 'center'
    },
    avatarImage: {
        width: SIZE.font20 * 3,
        height: SIZE.font20 * 3,
        borderRadius: SIZE.font20 * 3 / 2,
        borderWidth: 3,
        borderColor: "green",
        overflow: "hidden"
    },
    stars: {
        flexDirection: 'row',
        paddingTop: SIZE.font6
    },
    cal2row1:{
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        flex: 2
    },
    description:{
        flex: 10,
        paddingTop: SIZE.font6
    },
    cal2row2:{
        flex: 1,
        flexDirection: 'row'
    },
    row2col1:{
        flex: 7,
        flexDirection: 'row'
    },
    row2col2:{
        flex: 5,
        flexDirection: 'row'
    },
    icon:{

    },
    title: {
        paddingHorizontal: SIZE.font6,
        // justifyContent: "center"
    },
    titleText:{
        color: COLORS.black,
        fontSize: SIZE.font10
    },
    nameText:{
        color: COLORS.black,
        fontSize: SIZE.font16
    },

 });
 
 export default Index;
 