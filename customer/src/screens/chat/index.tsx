import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  View,
  Text,
  TextInput,
  Pressable,
} from 'react-native';
import {SafeAreaView} from 'react-native-safe-area-context';

import {COLORS} from '../../theme/colors';
import {SIZE} from '../../theme/fonts';
import metrics from '../../theme/metrics';
import {SendIcon} from '../../containers/icons';

const App = () => {
  const [chat, setChat] = useState('');
  const _handleChange = (value: any) => {
    setChat(value);
  };
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={styles.container}>
        <View style={styles.body}>
          <View style={{alignItems: 'flex-start'}}>
            <View style={styles.leftAlineView}>
              <View>
                <View>
                  <Text style={styles.chatText}>How are you?</Text>
                </View>
                <View style={{alignItems: 'flex-end'}}>
                  <Text style={styles.chatTimeText}>30/03/2022 4:30</Text>
                </View>
              </View>
            </View>
          </View>
          <View style={{alignItems: 'flex-end'}}>
            <View style={styles.rightAlineView}>
              <View style={{alignItems: 'flex-end'}}>
                <Text style={styles.chatText}>Fine</Text>
                <Text style={styles.chatText}>But you?</Text>
              </View>
              <View>
                <Text style={styles.chatTimeText}>30/03/2022 8:30</Text>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.chatInputCon}>
          <View style={styles.inputView}>
            <TextInput
              style={styles.TextInput}
              placeholder="Message"
              placeholderTextColor="#003f5c"
              onChangeText={_handleChange}
            />
          </View>
          <View>
            {chat === '' ? (
              <View
                style={{
                  borderRadius: (SIZE.font14 * 3) / 2,
                  height: SIZE.font14 * 3,
                  width: SIZE.font14 * 3,
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderColor: COLORS.orange,
                  borderWidth: 1,
                }}>
                <SendIcon color={COLORS.orange} />
              </View>
            ) : (
              <Pressable
                style={{
                  borderRadius: (SIZE.font14 * 3) / 2,
                  height: SIZE.font14 * 3,
                  width: SIZE.font14 * 3,
                  backgroundColor: COLORS.orange,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <SendIcon />
              </Pressable>
            )}
          </View>
        </View>
      </View>
    </SafeAreaView>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'flex-end',
  },
  body: {},
  chatInputCon: {
    height: SIZE.font10 * 6,
    backgroundColor: COLORS.primery,
    borderTopWidth: 1,
    borderTopColor: COLORS.orange,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: SIZE.font6,
  },
  inputView: {
    borderWidth: 1,
    borderColor: COLORS.orange,
    borderRadius: (SIZE.font14 * 3) / 2,
    width: metrics.screenWidth * 0.8,
    height: SIZE.font14 * 3,
  },
  TextInput: {
    flex: 1,
    padding: SIZE.font10,
    marginLeft: 20,
  },
  leftAlineView: {
    backgroundColor: COLORS.primery,
    margin: SIZE.font6,
    padding: SIZE.font6,
    borderRadius: SIZE.font6,
    width: '80%',
  },
  rightAlineView: {
    backgroundColor: '#faebd7',
    margin: SIZE.font6,
    padding: SIZE.font6,
    borderRadius: SIZE.font6,
    width: '80%',
  },
  chatTimeText: {
    fontSize: SIZE.font10,
  },
  chatText: {
    fontSize: SIZE.font14,
    color: COLORS.black,
  },
});
