const initialFields = {
    user_id: "",
    // call_session_id: "",
    receiver_id: "",
    sender_id: "",
    call_rate: "",
    name: "",
    birth_date: "1982-02-12",
    birth_time: "21:07:00",
    birth_place: "kol",
    phone_no: "9733611035"
}

const integerFilelds = ['user_id','call_session_id','receiver_id','sender_id','call_rate','phone_no']

export{
    initialFields, integerFilelds
}