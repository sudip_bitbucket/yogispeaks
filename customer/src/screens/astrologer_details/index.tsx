import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
  Pressable,
  TextInput,
} from 'react-native';
import screen from '../../theme/metrics';
import {COLORS} from '../../theme/colors';
import {SIZE, WEIGHT} from '../../theme/fonts';
import {
  Filter as FilterIcon,
  Down,
  Star,
  Language,
  ArrowRight,
  Rupee,
  Close,
  Staro,
} from '../../containers/icons';
import Button from '../../components/button';
import DatePicker from 'react-native-date-picker';
import {initialFields, integerFilelds} from './config';
import API from '../../utils/_axios';
import AsyncStorage from '@react-native-community/async-storage';
const axios = require('axios');

const Description: React.FC<{
  title: string;
}> = ({children, title}) => (
  <>
    <View style={styles.icon}>{children}</View>
    <View style={styles.title}>
      <Text style={styles.titleText}>{title}</Text>
    </View>
  </>
);
const Spelization: React.FC<{
  title: string;
}> = ({children, title}) => (
  <View style={styles.spelizationRow}>
    <Text style={styles.titleText}>{title}</Text>
  </View>
);

const Index = (props: any) => {
  const {consultantId, channel} = props.route.params;
  const [modalVisible, setModalVisible] = useState(false);
  const [imagePath, setImagePath] = useState('');
  const [profile, setProfile] = useState('');
  const [fields, setFields] = useState({...initialFields, receiver_id: consultantId});

  useEffect(() => {
    console.log('useEffect')
    _getConsultantList();
  }, []);

  const _getConsultantList = () => {
    axios({
      method: 'post',
      url: 'https://yogispeaks.co.in/Api/user_profile_details',
      data: {user_id: consultantId},
    }).then((response: any) => {
      if (response.data.status === '1') {
        setProfile(response.data.user_details);
        setImagePath(response.data.user_image_url);
      } else {
      }
    });
  };

  const _getSenderId = async () => {
    try {
      const value = await AsyncStorage.getItem('user_id');
        return value !== null ? value : null
    } catch (error) {
        console.log('error!',error)
      // Error retrieving data
    }
  };

  const _connectToConsultant = () => {
    setModalVisible(!modalVisible);
    // props.navigation.navigate('Chat')
    // props.navigation.navigate('VideoCall')
    _sendCallRequest();
  };

  const _sendCallRequest = async () => {
    const senderId = await _getSenderId()
    const fieldsObj = {
      ...fields,
      user_id: senderId,
      sender_id: senderId,
      call_rate: profile.call_rate
    };
    
      const response = await API.post('send_request_call_session', {...fieldsObj});
      if(!!response && response.data.status == 1)
        props.navigation.navigate('VideoCall', {uID: senderId, consultantId: consultantId, channel: response.data.call_session_id});
        // props.navigation.navigate('JoinChannelVideo', {uID: senderId, consultantId: consultantId, channel: response.data.call_session_id});
      else
        console.log(response.data.message)
  };

  const _closeCallSession = async () => {
    const response = await API.post('close_call_session', {call_session_id: channel});
    // if(!!response && response.data.status == 1)
        console.log(response.data)
    // else
    //     console.log(response.data.message)
  }

  const _handleChange = (name: any) => (value: any) => {
    let fieldValue = integerFilelds[name];
    if (integerFilelds.indexOf(name) !== -1) {
      if (isNaN(parseFloat(value))) {
        fieldValue = null;
      } else {
        fieldValue = parseInt(value, 10);
      }
    } else {
      fieldValue = value;
    }

    setFields({...fields, [name]: fieldValue});
  };

  if(channel !== null){
    _closeCallSession()
  }

  return (
    <SafeAreaView style={{flex: 1}}>
      <ScrollView style={{flex: 1, backgroundColor: COLORS.white}}>
        <View style={{flex: 5}}>
          <View style={styles.row}>
            <View style={styles.cal1}>
              <View style={styles.avatar}>
                <Image
                  style={styles.avatarImage}
                  source={{uri: imagePath + profile.profile_image}}
                />
              </View>
            </View>
            <View style={styles.cal2}>
              <View style={styles.cal2row1}>
                <View>
                  <Text style={styles.nameText}>
                    {profile.abbreviation +
                      ' ' +
                      profile.fname +
                      ' ' +
                      profile.lname}
                  </Text>
                </View>
              </View>
              <View style={styles.description}>
                <View style={styles.cal2row2}>
                  <View style={styles.row2col1}>
                    <Description title={profile.chat_rate + '/m'}>
                      <Image
                        style={{width: SIZE.font20, height: SIZE.font20}}
                        source={require('../../assets/icons/chat.png')}
                      />
                    </Description>
                  </View>
                  <View style={styles.row2col2}>
                    <Description title={profile.call_rate + '/m'}>
                      <Image
                        style={{width: SIZE.font20, height: SIZE.font20}}
                        source={require('../../assets/icons/call.png')}
                      />
                    </Description>
                  </View>
                </View>
                <View style={styles.cal2row2}>
                  <View style={styles.row2col1}>
                    <Description title={profile.language_ids}>
                      <Image
                        style={{width: SIZE.font20, height: SIZE.font20}}
                        source={require('../../assets/icons/language.png')}
                      />
                    </Description>
                  </View>
                  <View style={styles.row2col2}>
                    <Description title={profile.experiance}>
                      <Image
                        style={{width: SIZE.font20, height: SIZE.font20}}
                        source={require('../../assets/icons/experience.png')}
                      />
                    </Description>
                  </View>
                </View>
                <View style={styles.cal2row2}>
                  <View style={styles.row2col1}>
                    <Description title={profile.consultant_expertise_ids}>
                      <Image
                        style={{width: SIZE.font20, height: SIZE.font20}}
                        source={require('../../assets/icons/astrology.png')}
                      />
                    </Description>
                  </View>
                  <View style={styles.row2col2}>
                    <View style={styles.stars}>
                      <Star />
                      <Star />
                      <Star />
                      <Star />
                      <Star />
                      {/* {
                                        [1,2,3,4,5].map((item,index) =>{
                                            const dd = parseInt(rating)
                                            return (!!dd && dd > index ? 
                                            <Star key={index} />
                                            :
                                            <Staro key={index} />)
                                        })
                                    } */}
                    </View>
                    <View style={styles.rateing}>
                      <Text style={styles.rateingText}>5</Text>
                    </View>
                  </View>
                </View>
              </View>
            </View>
          </View>
          <View style={styles.spelization}>
            <Spelization title="Birth Time Colsultant" />
            <Spelization title="Busness Colsultant" />
          </View>
        </View>
        <View
          style={{
            borderBottomColor: 'gray',
            borderBottomWidth: 1,
            padding: SIZE.font6,
            marginHorizontal: SIZE.font6 * 2,
          }}
        />
        <View style={{flex: 2}}>
          <View>
            <TouchableOpacity
              onPress={() => setModalVisible(true)}
              style={styles.callButton}>
              <View>
                <Image
                  style={{width: SIZE.font20, height: SIZE.font20}}
                  source={require('../../assets/icons/telephone.png')}
                />
              </View>
              <View>
                <Text style={styles.buttonText}>Start call</Text>
              </View>
              <View>
                <Text style={styles.buttonText}>50/m</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={{marginTop: SIZE.font12}}>
          <View style={styles.aboutRow}>
            <View style={styles.aboutCol1}>
              <Text style={styles.nameText}>About Me</Text>
            </View>
            <View style={styles.aboutCol2}>
              <Text style={styles.titleText}>{profile.about}</Text>
            </View>
          </View>
        </View>
      </ScrollView>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <View style={styles.modalHeader}>
              <View>
                <Text style={styles.nameText}>
                  Call with Mr. Dr. Anower Siddha
                </Text>
              </View>
              <View>
                <Close onPress={() => setModalVisible(!modalVisible)} />
              </View>
            </View>
            <View style={styles.inputView}>
              <TextInput
                value={fields.name}
                style={styles.TextInput}
                placeholder="Name"
                placeholderTextColor="#003f5c"
                onChangeText={_handleChange('name')}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
              value={fields.phone_no}
                style={styles.TextInput}
                placeholder="Phone"
                placeholderTextColor="#003f5c"
                onChangeText={_handleChange('phone_no')}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
              value={fields.birth_date}
                style={styles.TextInput}
                placeholder="DD-MM-YYYY"
                placeholderTextColor="#003f5c"
                onChangeText={_handleChange('birth_date')}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
              value={fields.birth_time}
                style={styles.TextInput}
                placeholder="..:.."
                placeholderTextColor="#003f5c"
                onChangeText={_handleChange('birth_time')}
              />
            </View>
            <View style={styles.inputView}>
              <TextInput
              value={fields.birth_place}
                style={styles.TextInput}
                placeholder="Birth Place"
                placeholderTextColor="#003f5c"
                onChangeText={_handleChange('birth_place')}
              />
            </View>
            <Button
              handleAction={() => _connectToConsultant()}
              title="Invite to call"
              viewStyle={{width: '100%'}}
            />
          </View>
        </View>
      </Modal>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  row: {
    flexDirection: 'row',
    margin: SIZE.font6,
    padding: SIZE.font6,
    flex: 8,
  },
  cal1: {
    justifyContent: 'center',
    alignItems: 'center',
    padding: SIZE.font6,
  },
  avatar: {
    backgroundColor: COLORS.white,
    width: SIZE.font20 * 5,
    height: SIZE.font20 * 5,
    borderRadius: SIZE.font10 * 5,
    borderWidth: 3,
    borderColor: 'green',
    justifyContent: 'center',
    alignItems: 'center',
  },
  avatarImage: {
    width: SIZE.font20 * 5,
    height: SIZE.font20 * 5,
    borderRadius: (SIZE.font20 * 5) / 2,
    borderWidth: 3,
    borderColor: 'green',
  },
  stars: {
    flexDirection: 'row',
    paddingTop: SIZE.font6,
  },
  cal2: {
    flex: 1,
    paddingHorizontal: SIZE.font6,
  },
  cal2row1: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 3,
  },
  nameText: {
    color: COLORS.black,
    fontSize: SIZE.font16,
  },
  description: {
    flex: 9,
    paddingTop: SIZE.font6,
  },
  cal2row2: {
    flex: 1,
    flexDirection: 'row',
  },
  row2col1: {
    flex: 6,
    flexDirection: 'row',
  },
  icon: {},
  title: {
    paddingHorizontal: SIZE.font6,
  },
  titleText: {
    color: COLORS.black,
    fontSize: SIZE.font10,
  },
  row2col2: {
    flex: 6,
    flexDirection: 'row',
  },
  rateing: {
    paddingHorizontal: SIZE.font6,
  },
  rateingText: {
    textAlign: 'center',
    fontSize: SIZE.font16,
  },
  spelization: {
    flexDirection: 'row',
    margin: SIZE.font6,
  },
  spelizationRow: {
    borderWidth: 1,
    borderColor: COLORS.orange,
    marginHorizontal: SIZE.font6,
    padding: SIZE.font6,
    borderRadius: SIZE.font20,
  },
  callButton: {
    flexDirection: 'row',
    marginHorizontal: SIZE.font6,
    marginTop: SIZE.font20,
    borderWidth: 1,
    borderColor: COLORS.callGrid,
    paddingVertical: SIZE.font10,
    paddingHorizontal: SIZE.font12,
    borderRadius: SIZE.font20,
    backgroundColor: COLORS.callGrid,
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: SIZE.font16,
    color: COLORS.white,
  },
  aboutRow: {
    paddingVertical: SIZE.font6,
    marginHorizontal: SIZE.font12,
    marginBottom: SIZE.font6,
  },
  aboutCol1: {},
  aboutCol2: {},
  modalHeader: {
    // backgroundColor: 'red',
    flexDirection: 'row',
    width: '100%',
    justifyContent: 'space-between',
    paddingBottom: SIZE.font12,
    // alignItems: "center"
  },

  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 22,
  },
  modalView: {
    // margin: 20,
    backgroundColor: COLORS.white,
    borderRadius: SIZE.font12,
    padding: SIZE.font12,
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5,
    width: screen.screenWidth - SIZE.font12 * 4,
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2,
  },
  //   buttonOpen: {
  //     backgroundColor: "#F194FF",
  //   },
  buttonClose: {
    backgroundColor: '#2196F3',
  },
  textStyle: {
    color: 'white',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  modalText: {
    marginBottom: 15,
    textAlign: 'center',
  },

  inputView: {
    // backgroundColor: "#FFC0CB",
    borderWidth: 1,
    borderColor: COLORS.orange,
    borderRadius: (SIZE.font14 * 3) / 2,
    width: '100%',
    height: SIZE.font14 * 3,
    // marginBottom: 40,
    // alignItems: "center",
    marginBottom: SIZE.font12,
  },
  TextInput: {
    // height: SIZE.font20 * 3,
    flex: 1,
    padding: SIZE.font10,
    marginLeft: 20,
  },
});

export default Index;
