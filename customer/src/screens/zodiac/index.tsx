
 import React, {useCallback} from 'react';
 import {
   SafeAreaView,
   StyleSheet,
   Text,
   View,
   Image,
   TouchableOpacity,
   Linking,
   Alert
 } from 'react-native';
import { SIZE } from '../../theme/fonts'
import screen from '../../theme/metrics'
import { COLORS } from '../../theme/colors';
const ariesURL= "https://yogispeaks.co.in/content/aries"
const tauresURL= "https://yogispeaks.co.in/content/taurus"
const geminiURL= "https://yogispeaks.co.in/content/gemini"
const cancerURL= "https://yogispeaks.co.in/content/cancer"
const leoURL= "https://yogispeaks.co.in/content/leo"
const virgoURL= "https://yogispeaks.co.in/content/virgo"
const libraURL= "https://yogispeaks.co.in/content/libra"
const scorpioURL= "https://yogispeaks.co.in/content/scorpio"
const sagittariusURL= "https://yogispeaks.co.in/content/sagittarius"
const capricornURL= "https://yogispeaks.co.in/content/capricorn"
const aquariusURL= "https://yogispeaks.co.in/content/aquarius"
const piscesURL= "https://yogispeaks.co.in/content/pisces"

const Grid: React.FC<{
    title: string
    icon: any,
    color: string,
    navigation: any,
    navigationPath: string,
    navigationType: string
  }> = ({children, title, icon, color, navigation, navigationPath, navigationType}) => {
    let iconPath = require('../../assets/icons/telephone.png')
      switch(icon){
        case 'aries':
            iconPath = require('../../assets/icons/aries.png')
        break
        case 'taures':
            iconPath = require('../../assets/icons/taures.png')
        break
        case 'gemini':
            iconPath = require('../../assets/icons/gemini.png')
        break
        case 'cancer':
            iconPath = require('../../assets/icons/cancer.png')
        break
        case 'leo':
            iconPath = require('../../assets/icons/leo.png')
        break
        case 'virgo':
            iconPath = require('../../assets/icons/virgo.png')
        break
        case 'libra':
            iconPath = require('../../assets/icons/libra.png')
        break
        case 'scorpio':
            iconPath = require('../../assets/icons/scorpio.png')
        break
        case 'sagittarius':
            iconPath = require('../../assets/icons/sagittarius.png')
        break
        case 'capricorn':
            iconPath = require('../../assets/icons/capricorn.png')
        break
        case 'aquarius':
            iconPath = require('../../assets/icons/aquarius.png')
        break
        case 'pisces':
            iconPath = require('../../assets/icons/pisces.png')
        break
      }

      const handlePress = useCallback(async () => {
        const supported = await Linking.canOpenURL(navigationPath);
        if (supported) {
          await Linking.openURL(navigationPath);
        } else {
          Alert.alert(`Don't know how to open this URL: ${navigationPath}`);
        }
      }, [navigationPath]);

    return (
        <TouchableOpacity onPress={navigationType === 'app' ? () => navigation.navigate(navigationPath) : handlePress} style={[styles.col, {backgroundColor: color}]}>
            <View style={{flex:3, justifyContent: 'center', }}><Image style={{width: SIZE.font16 * 4, height: SIZE.font16 * 4}} source={iconPath} /></View>
            <View style={{flex:1, justifyContent: 'center', alignItems: 'center'}}><Text style={styles.gridText}>{title}</Text></View>
        </TouchableOpacity>
    );
  };
 
 const Index = (props: any) => {
  const {navigation} = props
   return <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
                <View style={styles.body}>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath={ariesURL} navigationType="webside"  color={COLORS.orange} title="Aries" icon='aries' />
                        <Grid navigation={navigation} navigationPath={tauresURL} navigationType="webside" color={COLORS.orange} title="Taures" icon="taures"/>
                        <Grid navigation={navigation} navigationPath={geminiURL} navigationType="webside" color={COLORS.orange} title="Gemini" icon="gemini"/>
                    </View>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath={cancerURL} navigationType="webside" color={COLORS.orange} title="Cancer" icon="cancer"/>
                        <Grid navigation={navigation} navigationPath={leoURL} navigationType="webside" color={COLORS.orange} title="Leo" icon="leo"/>
                        <Grid navigation={navigation} navigationPath={virgoURL} navigationType="webside" color={COLORS.orange} title="Virgo" icon="virgo"/>
                    </View>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath={libraURL} navigationType="webside" color={COLORS.orange} title="Libra" icon="libra"/>
                        <Grid navigation={navigation} navigationPath={scorpioURL} navigationType="webside" color={COLORS.orange} title="Scorpio" icon="scorpio"/>
                        <Grid navigation={navigation} navigationPath={sagittariusURL} navigationType="webside" color={COLORS.orange} title="Sagittarius" icon="sagittarius"/>
                    </View>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath={capricornURL} navigationType="webside" color={COLORS.orange} title="Capricorn" icon="capricorn"/>
                        <Grid navigation={navigation} navigationPath={aquariusURL} navigationType="webside" color={COLORS.orange} title="Aquarius" icon="aquarius"/>
                        <Grid navigation={navigation} navigationPath={piscesURL} navigationType="webside" color={COLORS.orange} title="Pisces" icon="pisces"/>
                    </View>
                </View>
            </View>
       </SafeAreaView>
 };
 
 const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "space-between",
        backgroundColor: COLORS.primery
    },
    header:{
    },
    body:{
        padding: SIZE.font6,
    },
    footer:{
    },
    row:{
        flexDirection: "row"
    },
    col:{
        flex:1,
        backgroundColor: 'orange',
        margin: SIZE.font6,
        borderRadius: SIZE.font8,
        height: ((screen.screenWidth - (SIZE.font6 * 8)) / 3),
        justifyContent: 'center',
        alignItems: 'center',
        padding: SIZE.font6,
    },
    gridText: {
        color: COLORS.white,
        fontSize: SIZE.font12,
        textAlign: 'center',
    }
 });
 
 export default Index;
 