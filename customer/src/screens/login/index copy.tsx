
 import React from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
   Image,
   TextInput,
   TouchableOpacity
 } from 'react-native';
 import Icon from 'react-native-vector-icons/AntDesign';
 import {Colors} from 'react-native/Libraries/NewAppScreen';
import screen from '../../theme/metrics'
import {SIZE} from '../../theme/fonts'
import {COLORS} from '../../theme/colors'
import {ArrowRight} from '../../containers/icons'
 
 const App = ({ navigation }) => {
   const isDarkMode = useColorScheme() === 'dark';
 
   const backgroundStyle = {
     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
   };
 
   return (
     <SafeAreaView style={{ flex: 1}}>
       <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
       <View style={styles.container}>
         <View style={{flex:1}}>
         <View style={{
           backgroundColor: "#FF7E00",
          flex: 5,
          borderBottomLeftRadius: 500,
          // borderBottomRightRadius: 700,
          // borderBottomEndRadius: 10,
          borderBottomStartRadius: 150,
          // borderWidth: 2,
          borderRightColor: "#FF7E00",
          borderEndColor: "#FF7E00",
          borderTopColor: "#FF7E00",
          // borderBottomColor: '#000',
        
        }}>
         </View>
         <View style={{backgroundColor: "#fff", flex: 7}}>
         
         </View>
         </View>
         <View style={{
          //  backgroundColor: "#fff",
           top: (screen.screenWidth * .10),
          left: (screen.screenWidth * .05),
           width: (screen.screenWidth * .90 ),
           height: 300,
           position: 'absolute'
         }}>
              <View style={{
                alignItems: "center",
                justifyContent: "center",
              }}>
              <Image source={require("../../assets/images/logo.png")} />
              </View>
            <View style={
              { 
                marginTop: screen.screenWidth * .10,
                backgroundColor: "#fff",
                height: 400,
                borderRadius: SIZE.font10,
                padding: SIZE.font20,
              }
            }>
              <View style={
                {
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginBottom: 40
                }
              }>
              <View style={styles.inputView}>
                <TextInput
                  style={styles.TextInput}
                  placeholder="Phone"
                  placeholderTextColor="#003f5c"
                  // onChangeText={(email) => setEmail(email)}
                />
                
              </View>
              {/* <View> */}
              <TouchableOpacity style={
                {
                  height:45,
                  width: 45,
                  backgroundColor: "#FF7E00",
                  borderRadius: 45/2,
                  justifyContent: "center",
                  alignItems: "center"
                }
              }>
                  <Text style={
                    {
                      fontSize: 18,
                      color: "#fff"
                    }
                  }>Go</Text>
                </TouchableOpacity>
              {/* </View> */}
              </View>
              <View style={
                {
                  flexDirection: "row",
                  justifyContent: "space-evenly"

                }
              }>
              <View style={styles.inputViewPin}>
              <TextInput
                  style={styles.TextInputPin}
                  placeholder=""
                  placeholderTextColor="#003f5c"
                />
              </View>
              <View style={styles.inputViewPin}>
              <TextInput
                  style={styles.TextInputPin}
                  placeholder=""
                  placeholderTextColor="#003f5c"
                />
              </View>
              <View style={styles.inputViewPin}>
              <TextInput
                  style={styles.TextInputPin}
                  placeholder=""
                  placeholderTextColor="#003f5c"
                />
              </View>
              <View style={styles.inputViewPin}>
              <TextInput
                  style={styles.TextInputPin}
                  placeholder=""
                  placeholderTextColor="#003f5c"
                />
              </View>
              
              </View>
              <View style={
                {
                  justifyContent: "center",
                  alignItems: "center",
                  marginVertical: SIZE.font20
                }
              }>
                <TouchableOpacity style={
                  {
                    height:SIZE.font10 * 5,
                    width: "100%",
                    backgroundColor: COLORS.callGrid,
                    borderRadius: 45/2,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                    paddingHorizontal: SIZE.font20
                  }
                }  onPress={() => navigation.navigate('HomeTabs')} >
                  <Text style={
                    {
                      color: COLORS.white,
                      fontSize: SIZE.font16
                    }
                  }>SIGN IN</Text>
                  <ArrowRight />
                  {/* <Icon name="arrowright" size={SIZE.font20} color={COLORS.white} /> */}
                </TouchableOpacity>
              </View>
              <View style={
                {
                  justifyContent: "center",
                  alignItems: "center",
                  marginBottom: 20,
                }
              }><Text style={
                {
                  color: COLORS.black,
                  fontSize: SIZE.font12
                }
              }>Now to Yogi speaks? <Text style={
                {
                  color: "blue"
                }
              }>Please Register</Text></Text></View>
           </View>
           
         </View>
       </View>
     </SafeAreaView>
       
   );
 };
 
 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },

   container:{
    flex: 1,
    backgroundColor: "#fff"
   },
   body: {
    flex: 1,
    // backgroundColor: "red"
   },
   main: {
    flex: 1,
    backgroundColor: "green"
   },





   inputView: {
    // backgroundColor: "#FFC0CB",
    borderWidth: 1,
    borderColor: "#FF7E00",
    borderRadius: 30,
    width: "85%",
    height: 45,
    // marginBottom: 40,
    // alignItems: "center",
    
  },
  
  TextInput: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  },


  inputViewPin: {
    // backgroundColor: "#FFC0CB",
    borderWidth: 1,
    borderColor: "#FF7E00",
    borderRadius: 5,
    width: 45,
    height: 45,
    marginBottom: 20,
    // alignItems: "center",
  },
  TextInputPin: {
    height: 50,
    flex: 1,
    padding: 10,
    marginLeft: 20,
  }

 });
 
 export default App;
 