
 import React, {useCallback} from 'react';
 import {
   SafeAreaView,
   StyleSheet,
   Text,
   View,
   Image,
   TouchableOpacity,
   Linking,
   Alert
 } from 'react-native';
import { SIZE } from '../../theme/fonts'
import screen from '../../theme/metrics'
import { COLORS } from '../../theme/colors';
const kundliURL= "https://yogispeaks.co.in/kundali"
const matchURL= "https://yogispeaks.co.in/kundali-matching"
const babyNameURL= "https://yogispeaks.co.in/child-names"

const Grid: React.FC<{
    title: string
    icon: any,
    color: string,
    navigation: any,
    navigationPath: string,
    navigationType: string
  }> = ({children, title, icon, color, navigation, navigationPath, navigationType}) => {
    let iconPath = require('../../assets/icons/telephone.png')
      switch(icon){
        case 'telephone':
            iconPath = require('../../assets/icons/telephone.png')
        break
        case 'comment':
            iconPath = require('../../assets/icons/comment.png')
        break
        case 'kundli':
            iconPath = require('../../assets/icons/kundli.png')
        break
        case 'babynames':
            iconPath = require('../../assets/icons/babyface.png')
        break
        case 'jigsaw':
            iconPath = require('../../assets/icons/jigsaw.png')
        break
        case 'horoscope':
            iconPath = require('../../assets/icons/horoscope.png')
        break
      }

      const handlePress = useCallback(async () => {
        const supported = await Linking.canOpenURL(navigationPath);
        if (supported) {
          await Linking.openURL(navigationPath);
        } else {
          Alert.alert(`Don't know how to open this URL: ${navigationPath}`);
        }
      }, [navigationPath]);

    return (
        <TouchableOpacity onPress={navigationType === 'app' ? () => navigation.navigate(navigationPath) : handlePress} style={[styles.col, {backgroundColor: color}]}>
            <View style={{flex:2, justifyContent: 'center'}}><Image style={{width: SIZE.font12 * 4, height: SIZE.font12 * 4}} source={iconPath} /></View>
            <View style={{flex:1}}><Text style={styles.gridText}>{title}</Text></View>
        </TouchableOpacity>
    );
  };
 
 const Login = (props: any) => {
  const {navigation} = props
   return <SafeAreaView style={{flex: 1}}>
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={{width: screen.screenWidth, height: SIZE.font20 * 7}} source={require("../../assets/images/add_bnner.png")} />
                </View>
                <View style={styles.body}>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath="AstrologerList" navigationType="app"  color={COLORS.callGrid} title="Tolk to Consultant" icon='telephone' />
                        <Grid navigation={navigation} navigationPath="AstrologerList" navigationType="app" color={COLORS.chatGrid} title="Tolk to Consultant" icon="comment"/>
                        <Grid navigation={navigation} navigationPath={kundliURL} navigationType="webside" color={COLORS.kundaliGrid} title="Kundli" icon="kundli"/>
                    </View>
                    <View style={styles.row}>
                        <Grid navigation={navigation} navigationPath={babyNameURL} navigationType="webside" color={COLORS.predictionGrid} title="Baby Names" icon="babynames"/>
                        <Grid navigation={navigation} navigationPath={matchURL} navigationType="webside" color={COLORS.matchingGrid} title="Match Making" icon="jigsaw"/>
                        <Grid navigation={navigation} navigationPath="zodiac" navigationType="app" color={COLORS.zodiacGrid} title="Zodiac" icon="horoscope"/>
                    </View>
                </View>
                <View style={styles.footer}>
                    <Image style={{width: screen.screenWidth, height: SIZE.font20 * 7}} source={require("../../assets/images/add_bnner.png")} />
                </View>
            </View>
       </SafeAreaView>
 };
 
 const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: "space-between",
    },
    header:{
    },
    body:{
        padding: SIZE.font6,
    },
    footer:{
    },
    row:{
        flexDirection: "row"
    },
    col:{
        flex:1,
        backgroundColor: 'orange',
        margin: SIZE.font6,
        borderRadius: SIZE.font8,
        height: ((screen.screenWidth - (SIZE.font6 * 8)) / 3),
        justifyContent: 'center',
        alignItems: 'center',
        padding: SIZE.font6,
    },
    gridText: {
        color: COLORS.white,
        fontSize: SIZE.font12,
        textAlign: 'center',
    }
 });
 
 export default Login;
 