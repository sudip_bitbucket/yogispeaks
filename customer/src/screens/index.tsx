import Login from "./home";
import Home from "./home"
import AstrologerList from "./astrologer_list"
import AstrologerDetails from "./astrologer_details"
import Zodiac from "./zodiac"
import Chat from "./chat"
import VideoCall from "./videoCall/homeScreen"
import JoinChannelVideo from "./videoCall/JoinChannelVideo"

export {
    Login,
    Home,
    AstrologerList,
    AstrologerDetails,
    Zodiac,
    Chat,
    VideoCall,
    JoinChannelVideo
}