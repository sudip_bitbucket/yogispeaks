import * as React from 'react';
import { View, Button, Image, Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import {HomeIcon, Chat, Phonecall, Wallet, Headphones, Search, Share, Bell } from '../containers/icons'
import Login from "../screens/login"
import { SIZE } from '../theme/fonts';
import Home from '../screens/home'
import { COLORS } from '../theme/colors';
import {AstrologerList} from '../screens'

const FeedScreen = ({ navigation }) =>{
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Button
        title="Go to Settings"
        onPress={() => navigation.navigate('Settings')}
      />
    </View>
  );
}

function ProfileScreen() {
  return <View />;
}

function AccountScreen() {
  return <View />;
}

function SettingsScreen() {
  return <View />;
}

function Dsone() {
  return <View />;
}

function Dstwo() {
  return <View />;
}


const Tab = createBottomTabNavigator();
function HomeTabs() {
  return (
    <Tab.Navigator 
        initialRouteName='Feed'
        
        screenOptions={{ 
        headerShown: false,
        tabBarStyle: { height: SIZE.font10 * 6 },
         }}>
      <Tab.Screen
        name="Feed"
        component={Home}
        options={{
            tabBarShowLabel: false,
            tabBarIcon: ()=> <HomeIcon />
        }}
        />
        <Tab.Screen
        name="Profile"
        component={FeedScreen}
        options={{
            tabBarIcon: ()=> <Chat />
        }}
        />
        <Tab.Screen
        name="Account"
        component={FeedScreen}
        options={{
            tabBarIcon: ()=> <Phonecall />
        }}
        />
        <Tab.Screen
        name="Dsone"
        component={Dsone}
        options={{
            tabBarIcon: ()=> <Wallet />
        }}
        />
        <Tab.Screen
        name="Dstwo"
        component={Dsone}
        options={{
            tabBarLabel: '',
            tabBarIcon: ()=> <Headphones />
        }}
        />
      {/* <Tab.Screen name="Profile" component={ProfileScreen} />
      <Tab.Screen name="Account" component={AccountScreen} /> */}
    </Tab.Navigator>
  );
}


const Stack = createNativeStackNavigator();
export default function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator 
        initialRouteName="Home"
        screenOptions={{
          headerTintColor: 'white',
          headerStyle: { 
              backgroundColor: COLORS.red,
            //   elevation: 0,
            //   shadowOpacity: 0
             },
            //  headerStatusBarHeight: 0
        }}
      >
        <Stack.Screen options={{headerShown: false}} name="Login" component={Login} />
        <Stack.Screen 
        name="Home" 
        component={HomeTabs}
        
        options={{
            // headerTintColor: "red"
            headerTitle: '',
            backImage: () =><Image source={require("../assets/images/logo.png")} />,
            headerLeft: () => {},
            headerRight: () => 
                <View style={
                    {
                        flexDirection: "row",
                        justifyContent: "space-between",
                    }
                }>
                    <View style={
                        {
                            alignItems: "flex-start",
                            justifyContent: "flex-start"
                        }
                    }><Image style={{ height: SIZE.font20}} source={require("../assets/images/logo.png")} /></View>
                    <View style={
                            {
                                flexDirection: "row"
                            }
                        }>
                        <TouchableOpacity style={{marginLeft: SIZE.font12}}><Bell /></TouchableOpacity>
                        <TouchableOpacity style={{marginLeft: SIZE.font12}}><Share /></TouchableOpacity>
                        <TouchableOpacity style={{marginLeft: SIZE.font12}}><Search /></TouchableOpacity>
                    </View>
                </View>
        }}
         /> 
        <Stack.Screen name="Settings" component={SettingsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}