import * as React from 'react';
import {View, Button, Image, Text, TouchableOpacity} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {
  HomeIcon,
  Chat as ChatIcon,
  Phonecall,
  Wallet,
  Headphones,
  Search,
  Share,
  Bell,
} from '../containers/icons';
import Login from '../screens/login';
import {SIZE} from '../theme/fonts';
import Home from '../screens/home';
import {COLORS} from '../theme/colors';
import {AstrologerList, AstrologerDetails, Zodiac, Chat, VideoCall, JoinChannelVideo} from '../screens';

function SettingsScreen() {
  return <View />;
}

function Dsone() {
  return <View />;
}

const Tab = createBottomTabNavigator();
function HomeTabs() {
  return (
    <Tab.Navigator
      initialRouteName="HomeNab"
      screenOptions={{
        headerShown: false,
        tabBarStyle: {height: SIZE.font10 * 6},
        tabBarActiveTintColor: COLORS.orange,
      }}>
      <Tab.Screen
        name="HomeNab"
        component={HomeNab}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <HomeIcon color={color} />,
        }}
      />
      <Tab.Screen
        name="message"
        component={ChatNab}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <ChatIcon color={color} />,
        }}
      />
      <Tab.Screen
        name="call"
        component={CallNab}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Phonecall color={color} />,
        }}
      />
      <Tab.Screen
        name="Dsone"
        component={Dsone}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Wallet color={color} />,
        }}
      />
      <Tab.Screen
        name="Dstwo"
        component={Dsone}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Headphones color={color} />,
        }}
      />

      {/* <Tab.Screen name="Profile" component={ProfileScreen} />
      <Tab.Screen name="Account" component={AccountScreen} /> */}
    </Tab.Navigator>
  );
}

const headerRight = () => {
  return (
    <View
      style={{
        flexDirection: 'row',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}>
        <Image source={require('../assets/images/logo.png')} />
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <TouchableOpacity style={{marginLeft: SIZE.font12}}>
          <Bell />
        </TouchableOpacity>
        <TouchableOpacity style={{marginLeft: SIZE.font12}}>
          <Share />
        </TouchableOpacity>
        <TouchableOpacity style={{marginLeft: SIZE.font12}}>
          <Search />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const screenOptions={
  headerTintColor: 'white',
  headerStyle: {
    backgroundColor: COLORS.orange,
  }
}

const Stack = createNativeStackNavigator();
const HomeNab = () => {
  return (
    <Stack.Navigator
      // headerMode="screen"
      screenOptions={{
        headerTintColor: 'white',
        headerStyle: {
          backgroundColor: COLORS.orange,
          // elevation: 0,
          // shadowOpacity: 0,
        },
        // headerStatusBarHeight: 0,
      }}>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      {/* <Stack.Screen
        name="AstrologerList"
        component={AstrologerList}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      <Stack.Screen
        name="AstrologerDetails"
        component={AstrologerDetails}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      <Stack.Screen
        name="zodiac"
        component={Zodiac}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Headphones color={color} />,
        }}
      /> */}
    </Stack.Navigator>
  );
};

const ChatNab = () => {
  return (
    <Stack.Navigator
      screenOptions={{...screenOptions}}>
      <Stack.Screen
        name="AstrologerList"
        component={AstrologerList}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      <Stack.Screen
        name="AstrologerDetails"
        component={AstrologerDetails}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      {/* <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarShowLabel: false,
          tabBarIcon: ({color, size}) => <Headphones color={color} />,
        }}
      /> */}
    </Stack.Navigator>
  );
};

const CallNab = () => {
  return (
    <Stack.Navigator
      screenOptions={{...screenOptions}}>
      <Stack.Screen
        name="AstrologerList"
        component={AstrologerList}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      <Stack.Screen
        name="AstrologerDetails"
        component={AstrologerDetails}
        options={{
          headerTitle: '',
          headerRight: () => headerRight(),
        }}
      />
      
    </Stack.Navigator>
  );
};

export default function index() {
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{...screenOptions}}>
        <Stack.Screen
          options={{headerShown: false}}
          name="Login"
          component={Login}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="HomeTabs"
          component={HomeTabs}
        />
        <Tab.Screen
          name="Chat"
          component={Chat}
          options={{
            tabBarShowLabel: false,
            tabBarIcon: ({color, size}) => <Headphones color={color} />,
          }}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="VideoCall"
          component={VideoCall}
        />
        <Stack.Screen
          options={{headerShown: false}}
          name="JoinChannelVideo"
          component={JoinChannelVideo}
        />
        <Stack.Screen name="Settings" component={SettingsScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
