import * as React from 'react'
import Icon from 'react-native-vector-icons/Ionicons';
import {COLORS} from "../../theme/colors"
import {SIZE} from "../../theme/fonts"

const Home:React.FC<{
    size?: number ,
    color?: string,
    onPress: () => {}
  }> = ({children, size, color, onPress}) =>
    <Icon 
    name="close" 
    size={size || SIZE.font14 * 2} 
    color={color || COLORS.black} 
    onPress={onPress}
    />


export default Home