import * as React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome';
import {COLORS} from "../../theme/colors"
import {SIZE} from "../../theme/fonts"

const Home:React.FC<{
    size?: number ,
    color?: string
  }> = ({children, size, color}) =>
    <Icon 
    name="send-o" 
    size={size || SIZE.font20} 
    color={color || COLORS.primery} />


export default Home