import ArrowRight from './arrowright'
import HomeIcon from './home'
import Chat from './chat'
import Headphones from './headphones'
import Phone from './phone'
import Phonecall from './phonecall'
import Wallet from './wallet'
import Share from './share'
import Bell from './bell'
import Search from './search'
import Menu from './menu'
import Filter from './filter'
import Down from './down'
import Star from './star'
import Staro from './staro'
import Language from './language'
import Right from './right'
import Rupee from './rupee'
import Close from './close'
import SendIcon from './send'

export {
    ArrowRight,
    HomeIcon,
    Chat,
    Headphones,
    Phone,
    Phonecall,
    Wallet,
    Share,
    Bell,
    Search,
    Menu,
    Filter,
    Down,
    Star,
    Language,
    Right,
    Rupee,
    Close,
    Staro,
    SendIcon
}