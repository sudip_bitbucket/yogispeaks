export const COLORS = {
    primery: "#FFFFFF",
    secondary: "#000000",
    textPrimery: "#000000",
    textGray: "",
    broderColor: "",
    backgroundColor: "#FFFFFF",
    ratingColor: "#FF7E00",
    progressGray: "",
    orange: "#FF7E00",
    red: "#860A04",
    callGrid: "#E50E0E",
    chatGrid: "#0CC784",
    kundaliGrid: "#037DBE",
    predictionGrid: "#8F0792",
    matchingGrid: "#D3A02A",
    zodiacGrid: "#269C48",
    white: "#ffffff",
    black: "#000000",
    gray: "#F0F0F0"

}

export default {COLORS}